import React from 'react';
import LanguageContext from '../contexts/LanguageContext';

class BeerName extends React.Component {
    static contextType = LanguageContext;
    render (){
        const text = this.context === 'hungarian' ? 'Magyar sor' : 'Deutsches bier';
        return(
            
                <div className = "ui field">
                    {text}
                </div>
                
                
            );
};
}

export default BeerName;
