import React from 'react';
import LanguageContext from '../contexts/LanguageContext';


class BeerButton extends React.Component {

    renderBeerButton(value) {
        return value === 'hungarian' ? 'Magyar gomb' : 'Deutscher knopf';
    }

  render (){
    return(
        <div>
        <button className="ui button primary">
            <LanguageContext.Consumer>
                { (value) => this.renderBeerButton(value)}
            </LanguageContext.Consumer>
            
        </button>
        
        </div>);
};
}

export default BeerButton;
