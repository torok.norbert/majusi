import React from 'react';
import OrderinForm from './components/OrderinForm';
import LanguageContext from './contexts/LanguageContext';

class App extends React.Component {

  state = {
    language: 'hungarian'
  }

  onLanguageChange =(language) =>{
    this.setState({
      language:language
    })
  }
  render (){
    return(
    <div>
     <div className="ui container"></div>
      Choose a language:
      <i className="flag hu"   onClick={() => {this.onLanguageChange('hungarian')}} /> 
      <i className="flag de"   onClick={() => {this.onLanguageChange('deutsch')}} /> 
      {this.state.language}
      <LanguageContext.Provider value = {this.state.language}>
      <OrderinForm></OrderinForm>
      </LanguageContext.Provider>
      <LanguageContext.Provider value = {'hungarian'}>
      <OrderinForm></OrderinForm>
      </LanguageContext.Provider>
 
      <OrderinForm></OrderinForm>

       
      
    </div>);
};
}

export default App;
